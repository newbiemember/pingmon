#     Simple Ping Mon
      Installed On Ubuntu 14.04
      Simple Script for monitoring server using ping method.
      If ping failed, the script automatically send email to admin.
##    Environment
      Postfix + mandrill
      PHP 5.6.16-2+deb.sury.org~trusty+1 (cli)
###   Config
      Install SMPTP on server monitoring & configure with API mandrill (for setup search on google).
      For looping check, Im use bash script. You can Use cron tab.
      For Bash Script running : "nohup ping.sh>/dev/null 2>&1 &"

